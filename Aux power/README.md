This product is made available under the terms of the TAPR Open Hardware License. See the LICENSE.TXT file that accompanies this distribution for the full text of the license.

**********************

Repository: www.github.com/eez-open
Web site: www.envox.hr/eez

**********************

EEZ 2-layer (+tKeepout +_tNames) job script is used for generating Gerber files. Use gerbv, a free/open source Gerber viewer (http://gerbv.geda-project.org/)
