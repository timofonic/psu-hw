[Transient Analysis]
{
   Npanes: 4
   Active Pane: 2
   {
      traces: 1 {524290,0,"V(direct)"}
      X: ('m',0,0,0.05,0.5)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {524292,0,"V(soft-start)"}
      X: ('m',0,0,0.05,0.5)
      Y[0]: (' ',1,0,0.5,5)
      Y[1]: (' ',1,1e+308,0.6,-1e+308)
      Volts: (' ',0,0,1,0,0.5,5)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 2 {524291,0,"V(vout)"} {589837,0,"V(vin)"}
      X: ('m',0,0,0.05,0.5)
      Y[0]: (' ',0,-360,60,360)
      Y[1]: (' ',0,1e+308,3,-1e+308)
      Volts: (' ',0,0,0,-360,60,360)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 2 {34668550,0,"I(L1)"} {34603013,0,"I(C1)"}
      X: ('m',0,0,0.05,0.5)
      Y[0]: (' ',0,-25,5,30)
      Y[1]: (' ',0,1e+308,8,-1e+308)
      Amps: (' ',0,0,0,-25,5,30)
      Log: 0 0 0
      GridStyle: 1
   }
}
