[Transient Analysis]
{
   Npanes: 2
   {
      traces: 1 {524290,0,"V(vpreg)"}
      X: ('m',0,0,0.05,0.485)
      Y[0]: (' ',0,3,3,30)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,3,3,30)
      Log: 0 0 0
      GridStyle: 1
   },
   {
      traces: 1 {589837,0,"V(vout+)"}
      X: ('m',0,0,0.05,0.485)
      Y[0]: (' ',0,0,2,26)
      Y[1]: ('_',0,1e+308,0,-1e+308)
      Volts: (' ',0,0,0,0,2,26)
      Log: 0 0 0
      GridStyle: 1
   }
}
